(ns game-of-life
  (:require [clojure.string :as str]))

(def usage
  "Användning: game-of-life [-fFIL]

Visa en Game of Life simulering med FIL som utgångspunkt.
")

(defn acquire-filename []
  "Gets filename to use as start-point."
  (doseq [arg *command-line-args*]
    (case arg
      "-f" (apply str (nthrest arg 2))
      (do (printf usage)))))

(defn parse-input [file-name]
  "Reads its argument and returns a useful game board."
  (str/split-lines
   (slurp file-name)))

(defn board->string [board]
  "Game board as a string."
  (str/join "\n"
          (map (fn [line]
                 (reduce (fn [s val]
                           (str s (if val "x" " "))) "" line)) board)))

(defn string->board [s]
  "String as a game board.

The space character is considered empty, everything else is considered
populated."
  (vec (for [line (str/split s #"\n")]
         (vec (map (fn [c] (not (= c \space))) line)))))


(defn -main []
  0)
